#!/bin/bash

#
# Package: lvmprovision
#
# Script: static_config.sh
#
# Contains definitions shared by all projects. You can override them in your
# project's configuration file.
#
# Naming conventions:
# * *_rel* prefix: Relative path.
# * *_abs* prefix: Absolute path.

# Ensure script robustness
# ========================
set -o errexit	# Fail immediatelly if any command fails.
set -o pipefail	# Fail immediatelly if any pipe stage fails.
set -o nounset	# Fail immediatelly if there is a derreference attempt on an unset variable. 

#
# Section: Tools
# External tools used by the different scripts.
#

# Variable: AWK
# Absolute path to the awk program.
export AWK="/usr/bin/awk"

# Variable: DIALOG
# Absolute path to the dialog program.
export DIALOG="/usr/bin/dialog"

# Variable: JO
# Absolute path to the jo program.
export JO="/usr/bin/jo"

# Variable: JQ
# Absolute path to the jq program.
export JQ="/usr/bin/jq"

# Variable: RSYNC
# Absolute path to the rsync program.
export RSYNC="/usr/bin/rsync"

# Variable: SHA
# Absolute path to the sha512 program.
export SHA="/usr/bin/sha512sum"

# Variable: SSH
# Absolute path to the ssh program.
export SSH="/usr/bin/ssh"

#
# Section: Relative paths
# Variables pointing relative paths.
#

# Variable: lib_lvmprovision_lib_name
# Name of the directory containing the lvm-provision library.
lib_lvmprovision_lib_name="lvm-provision"

# Variable: lib_script_dir_rel
# Directory of the lvm-provision scripts. Relative to the lvm-provision library root.
lib_script_dir_rel="sh"

# Variable: lib_script_utils_rel
# utils.sh script. Relative to the lvm-provision library root.
lib_script_utils_rel="${lib_script_dir_rel}/utils.sh"

# Variable: lib_script_local_dir_rel
# Directory of the lvm-provision local scripts. Relative to the lvm-provision library root.
lib_script_local_dir_rel="${lib_script_dir_rel}/local"

# Variable: lib_script_local_lvmprovision_rel
# Local lvm-provision.sh script. Relative to the lvm-provision library root.
lib_script_local_lvmprovision_rel="${lib_script_local_dir_rel}/lvm-provision.sh"

# Variable: lib_script_local_provisionmetadata_rel
# Local provision-metadata.sh script. Relative to the lvm-provision library root.
lib_script_local_provisionmetadata_rel="${lib_script_local_dir_rel}/provision-metadata.sh"

# Variable: lib_script_remote_dir_rel
# Directory of the lvm-provision remote scripts. Relative to the lvm-provision library root.
lib_script_remote_dir_rel="${lib_script_dir_rel}/remote"

# Variable: project_ssh_dir_rel
# Name of the directory containing the SSH keys.
project_ssh_dir_rel=ssh

# Variable: project_config_rel
# Name of the file containing the project configuration.
export project_config_rel=config.sh

# Variable: project_script_dir_rel
# Name of the directory containing the scripts to execute.
export project_script_dir_rel=sh

# Variable: project_ssh_private_key_rel
# Name of the file with the private SSH key.
export project_ssh_private_key_rel="${project_ssh_dir_rel}/id_rsa"

#
# Section: Remote paths
# Variables pointing paths absolute to the remote host.
#

# Variable: remote_lib_dir_abs
# Root directory of the lvm-provision library on the remote host. Absolute.
remote_lib_dir_abs="$(realpath "$(dirname "${0}")/../..")"

# Variable: remote_lib_script_dir_abs
# Directory of the lvm-provision scripts on the remote host. Absolute.
export remote_lib_script_dir_abs="${remote_lib_dir_abs}/${lib_script_dir_rel}"

# Variable: remote_lib_script_utils_abs
# Absolute path to the utils.sh script on the remote host.
export remote_lib_script_utils_abs="${remote_lib_dir_abs}/${lib_script_utils_rel}"

# Variable: remote_lib_script_local_dir_abs
# Directory of the lvm-provision local scripts on the remote host. Absolute.
export remote_lib_script_local_dir_abs="${remote_lib_dir_abs}/${lib_script_local_dir_rel}"

# Variable: remote_lib_script_remote_dir_abs
# Directory of the lvm-provision remote scripts on the remote host. Absolute.
export remote_lib_script_remote_dir_abs="${remote_lib_dir_abs}/${lib_script_remote_dir_rel}"

#
# Section: Local paths
# Variables pointing paths absolute to the local host.
#

# Variable: local_script_dir_abs
# Absolute path to the directory where lvm-provision scripts and project scripts will be copied.
local_script_dir_abs=/root/lvm-provision-scripts

# Variable: local_lvm_provision_metadata_dir_abs
# Absolute path to the directory where lvm-provision metadate is stored.
export local_lvm_provision_metadata_dir_abs=/root/.lvm-provision

# Variable: local_lib_script_dir_abs
# Absolute path to the lvm-provision library on the local host.
export local_lib_script_dir_abs="${local_script_dir_abs}/${lib_lvmprovision_lib_name}"

# Variable: local_lib_script_utils_abs
# Absolute path to the utils.sh script on the local host.
export local_lib_script_utils_abs="${local_lib_script_dir_abs}/${lib_script_utils_rel}"

# Variable: local_lvm_provision_local_abs
# Absolute path to the lvm-provision.sh script on the local host.
export local_lvm_provision_local_abs="${local_lib_script_dir_abs}/${lib_script_local_lvmprovision_rel}"

# Variable: local_lib_script_local_provisionmetadata_abs
# Absolute path to the provision-metadata.sh script on the local host
export local_lib_script_local_provisionmetadata_abs="${local_lib_script_dir_abs}/${lib_script_local_provisionmetadata_rel}"

#
# Section: Misc
# Some other variables.
#

# Variable: pause_seconds
# Number of seconds to pause script execution when invoking the "pause" function.
export pause_seconds=30

