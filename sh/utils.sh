#!/bin/bash

#
# Package: lvmprovision
#
# Script: utils.sh
#
# Defines utility functions used in other scripts.
#

# Ensure script robustness
# ========================
set -o errexit	# Fail immediatelly if any command fails.
set -o pipefail	# Fail immediatelly if any pipe stage fails.
set -o nounset	# Fail immediatelly if there is a derreference attempt on an unset variable. 

#
# Function: check_environment
#
# Checks whether all symbols given as arguments correspond to a variable in the environment. This is done by using indirect parameter expansion: "${!var}"
#
# Since I'm using 'nounset' on all scripts, this function is of lesser importance. I'm keeping it anyway for future reference.
#
# Parameters:
#
#   ${@} - Variable names to resolve.
#
# Returns:
#
# 0 if everything is OK. Non-0 otherwise.
#
function check_environment {
	local ___all_ok=0

	# Iterate over all names.
	for environment_variable in "${@}" ; do
		if [[ "${!environment_variable}x" = "x" ]] ; then
			echo "Varible '${environment_variable}' not found in the environment." 1>&2
			___all_ok=1
		fi
	done

	return "${___all_ok}"
}

#
# Function: check_required_programs
#
# Checks whether all strings given as arguments correspond to an executable program on the system.
#
# Parameters:
#
#   ${@} - Program paths to check.
#
# Returns:
#
# 0 if everything is OK. Non-0 otherwise.
#
function check_required_programs {
	required_programs_ok=0
	for required_program in "${@}" ; do
		if [[ ! -x "${required_program}" ]] ; then
			echo "Required program '${required_program}' is missing. Please, install it or provide its correct location through configuration file." 1>&2
			required_programs_ok=1
		fi
	done

	return "${required_programs_ok}"
}

