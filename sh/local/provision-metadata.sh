#!/bin/bash

#
# Package: lvmprovision.local
#
# Script: provision-metadata.sh
#
# Provides functions to manage the metadata related to the provision process.
#
# State machine:
#
#>REGULAR FLOW
#>
#> .--------------.
#> |     101      |
#> | READY_TO_RUN |
#> '--------------'
#> Running |
#>         v
#>    .---------.
#>    |   102   |
#>    | RUNNING |---------------------.
#>    '---------'     Reboot required |
#> Success |                          |
#>         v                          v
#>.----------------.         .----------------.
#>|      103       | Success |      104       |
#>| PENDING_COMMIT |<--------| PENDING_REBOOT |
#>'----------------'         '----------------'
#> Success |
#>         v
#>   .-----------.
#>   |    105    |
#>   | COMMITTED |
#>   '-----------'
#>
#>MODIFICATIONS HANDLING
#>
#>.--------------.            .--------------.
#>|     101      |   Modified |     101      |
#>| READY_TO_RUN |----------->| READY_TO_RUN |<--------.Success
#>'--------------'            '--------------'         |
#>.---------.                               .--------------------.
#>|   102   |        Modified               |        202         |
#>| RUNNING |----------------------------.  | PENDING_LVM_REBOOT |
#>'---------'                            v  '--------------------'
#>.----------------.          .---------------------.  ^
#>|      103       | Modified |         201         |  | Success
#>| PENDING_COMMIT |--------->| PENDING_LVM_RESTORE |--'
#>'----------------'          '---------------------'
#>.----------------.                     ^
#>|      104       | Modified            |
#>| PENDING_REBOOT |---------------------|
#>'----------------'                     |
#>.-----------.                          |
#>|    105    |      Modified            |
#>| COMMITTED |--------------------------'
#>'-----------'
#>
#>ERROR HANDLING
#>
#>.--------------.       .----------------.       .--------------.
#>|     101      | Error |      301       | Fixed |     101      |
#>| READY_TO_RUN |------>| E_READY_TO_RUN |------>| READY_TO_RUN |
#>'--------------'       '----------------'       '--------------'
#>.----------------.
#>|      103       |      Error                           ^
#>| PENDING_COMMIT |----------------.                     |
#>'----------------'                v                     |
#>.---------------------.       .-------.                 |
#>|         201         | Error |  303  |   Fixed         |
#>| PENDING_LVM_RESTORE |------>| E_LVM |-----------------'
#>'---------------------'       '-------'
#>.--------------------.            ^
#>|        202         |  Error     |
#>| PENDING_LVM_REBOOT |------------'
#>'--------------------'
#>.---------.
#>|   102   |             Error
#>| RUNNING |-------------------------.
#>'---------'                         v
#>.----------------.            .----------.       .---------------------.
#>|      104       |      Error |   302    | Fixed |         201         |
#>| PENDING_REBOOT |----------->| E_SCRIPT |------>| PENDING_LVM_RESTORE |
#>'----------------'            '----------'       '---------------------'
#

# TODO: Use and parse "$(last reboot)" to get the reboot history (with details)
# TODO: I can't store the state in the same LV that I'm managing... It won't survive restores!
# TODO: Check that I'm not storing state in the same lv that I'm managing.
#

# Variable: __lpm_jq_common_params
# Global list that holds the parameters common to all jq calls in this script.
__lpm_jq_common_params=("--raw-output" "--exit-status")
readonly __lpm_jq_common_params
export __lpm_jq_common_params

#
# Function: lvm_provision_metadata_init
#
# Initializes LVM-provision metadata or ensures that it is properly initialized.
#
# Parameters:
#
#   ${1} - Absolute path to the directory containing tracking information for all projects.
#
#   ${2} - Identifier of the project to inialize.
#
# Returns:
#
#   - 0 if everything is OK. Non-0 otherwise.
#
#   - The configuration string. You must capture it and pass it to other lvm_provision_metadata functions.
#
# TODO:
# * Check that the LVG that contains the target LV has some free space to hold the snapshots.
# ** If 0, fail.
function lvm_provision_metadata_init {
	local_lvm_provision_metadata_dir_abs="${1}"
	project_id="${2}" 
	local config_dir_abs="${local_lvm_provision_metadata_dir_abs}/${project_id}"

	# Ensure that the configuration directory exists.
	if ! mkdir -p "${config_dir_abs}" ; then echo "Unable to create configuration directory '${config_dir_abs}'" 1>&2 ; return 1 ; fi

	local config_file_abs="${config_dir_abs}/config.json"

	if [[ ! -f "${config_file_abs}" ]] ; then
		JO_ARGS=()
		JO_ARGS+=("config_version=1")
		JO_ARGS+=("project_id=${project_id}")
		JO_ARGS+=("config_file_abs=${config_file_abs}")
		JO_ARGS+=("local_lvm_provision_metadata_dir_abs=${local_lvm_provision_metadata_dir_abs}")

		jo "${JO_ARGS[@]}" > "${config_file_abs}"
	fi

	if ! config_version="$(jq "${__lpm_jq_common_params[@]}" ".config_version" "${config_file_abs}")" ; then echo "Unable to get configuration version from file '${config_file_abs}'" 1>&2 ; return 1 ; fi

	if [[ "${config_version}" != 1 ]] ; then echo "Invalid config version '${config_version}'" 1>&2 ; return 1; fi
	
	cat "${config_file_abs}"
}
export lvm_provision_metadata_init

#
# Function: lvm_provision_metadata_run
#
# Executes the state machine until there are no new events.
#
# Parameters:
#
#   ${1} - Index of the script.
#
#   ${2} - Absolute path to the script.
#
#   ${3} - Starting event to process. Can be an empty string, but is not optional.
#
#   ${4} - Raw configuration.
#
# Return:
#
#  Return code - 0 if everything is OK. Non-0 otherwise.
#
#  Descriptor 3 - Name of the current state.
#
function lvm_provision_metadata_run {
	script_index="${1}"
	script_abs="${2}"
	external_event="${3}"
	raw_config="${4}"

	if [[ "${script_index}x" = "x" ]] || [[ "${script_abs}x" = "x" ]] || [[ "${raw_config}x" = "x" ]] ; then
		echo "One or more of 'script_index', 'script_abs' or 'raw_config' parameters are missing." 1>&2
		return 1
	fi

	if ! current_script_hash="$(sha512sum "${script_abs}" | awk '{print $1}' )" ; then
		echo "Unable to calculate hash of file '${script_abs}'." 1>&2
		return 1
	fi

	# Obtain the absolute directory to the project tracking information.
	local config_file_abs
	if ! config_file_abs="$(echo "${raw_config}" | jq "${__lpm_jq_common_params[@]}" ".config_file_abs")" ; then
		echo "Unable to obtain config file directory from raw data." 1>&2
		return 1
	fi

	# Get the tracking information for the current script.
	if ! jq "${__lpm_jq_common_params[@]}" ".scripts[${script_index}]" "${config_file_abs}" > /dev/null ; then
		# Prepare the configuration record for current script.
		local JO_ARGS
		JO_ARGS=()

		JO_ARGS+=("path_abs=${script_abs}")
		JO_ARGS+=("hash=${current_script_hash}")
		JO_ARGS+=("state=READY_TO_RUN")

		local __script_config
		if ! __script_config="$(jo "${JO_ARGS[@]}")" ; then
			echo "Unable to generate configuration for script with index ${script_index}." 1>&2
			return 1
		fi

		__lpm_update "${config_file_abs}" ".scripts[${script_index}]" "${__script_config}" || return 1
	fi

	# Check whether the script has been modified with respect the tracked version.
	local script_tracked_hash
	if ! script_tracked_hash="$(jq "${__lpm_jq_common_params[@]}" ".scripts[${script_index}].hash" "${config_file_abs}")" ; then
		echo "Unable to get script hash for index ${script_index}." 1>&2
		return 1
	fi
	local event
	if [[ "${script_tracked_hash}" = "${current_script_hash}" ]] ; then
		event="${external_event}"
	else
		event="modified"
	fi

	# Make sure the script path remains consistent with respect tracked data.
	if [[ "$(jq "${__lpm_jq_common_params[@]}" ".scripts[${script_index}].path_abs" "${config_file_abs}")" != "${script_abs}" ]] ; then
		echo "Updating script path."
		__lpm_update "${config_file_abs}" ".scripts[${script_index}].path_abs" "${script_abs}" || return 1
	fi

	# Process the state machine loop.
	local next_event
	local is_first_iteration
	is_first_iteration=true

	while [[ "${event}x" != "x" ]] || [[ "${is_first_iteration}" = true ]] ; do
		is_first_iteration=false

		# Determine state of the script.
		state="$(__lpm_get_state "${config_file_abs}" "${script_index}")"

		case "${state}" in
			READY_TO_RUN)
				if [[ "${event}" = "modified" ]] ; then
					__lpm_update "${config_file_abs}" ".scripts[${script_index}].hash" "${current_script_hash}" || return 1
				fi
				if [[ "${event}" =~ enter|do|modified ]] || [[ "${event}x" = "x" ]] ; then
					__lpm_update "${config_file_abs}" ".scripts[${script_index}].state" "\"RUNNING\"" || return 1
					next_event="enter"
				else
					# Unrecognized event
					echo "Unrecognized event '${event}'" 1>&2
					return 1
				fi
			;;
			RUNNING)
				case "${event}" in
					enter)
						local ret
						# TODO: log output.
						# Maybe log everything to syslog? ==> https://urbanautomaton.com/blog/2014/09/09/redirecting-bash-script-output-to-syslog/
						"${script_abs}"
						ret="${?}"

						case "${ret}" in
							1)
								__lpm_update "${config_file_abs}" ".scripts[${script_index}].state" "\"PENDING_COMMIT\"" || return 1
								next_event="enter"
							;;

							2)
								__lpm_update "${config_file_abs}" ".scripts[${script_index}].state" "\"PENDING_REBOOT\"" || return 1
								next_event="enter"
							;;

							*)
								__lpm_update "${config_file_abs}" ".scripts[${script_index}].state" "\"E_SCRIPT\"" || return 1
								next_event="enter"
							;;
						esac
					;;
					*)
						# Unrecognized event
						echo "Unrecognized event '${event}'" 1>&2
						return 1
					;;
				esac
			;;
			PENDING_COMMIT)
				echo "Not implemented!" 1>&2
				return 1
			;;
			PENDING_REBOOT)
				echo "Not implemented!" 1>&2
				return 1
			;;
			COMMITED)
				echo "Not implemented!" 1>&2
				return 1
			;;
			E_SCRIPT)
				case "${event}" in
					''|enter)
						# Unrecognized event
						echo "Script '${script_abs}' is in error state. Manual intervention is required." 1>&2
						return 1
					;;
					*)
						# Unrecognized event
						echo "Unrecognized event '${event}'" 1>&2
						return 1
					;;
				esac
			;;
			*)
				# Unrecognized state
				echo "Unrecognized state '${state}'" 1>&2
				return 1
			;;
		esac

		event="${next_event}"
		next_event=""
	done
}
export lvm_provision_metadata_run

#
# Section: Internal functions.
# Don't invoke the following functions outside this library.
#

#
# Function: __lpm_update
#
# Parameters:
#
#	${1} - Absolute path to the file to update.
#
#	${2} - jq query to select the position on which data will be inserted.
#
#	${3} - Data to insert.
#
# Returns:
#
#	0 if update was successful. Non-0 otherwise.
#

function __lpm_update {
	local config_file_abs
	config_file_abs="${1}"

	local jq_query
	jq_query="${2}"

	local json_data
	json_data="${3}"
	
	# Update the configuration.
	local JQ_ARGS
	JQ_ARGS=()

	JQ_ARGS+=("${__lpm_jq_common_params[@]}")
	JQ_ARGS+=("${jq_query} = ${json_data}")
	JQ_ARGS+=("${config_file_abs}")

	# Update the tracking information file.
	local jq_output
	if jq_output="$(jq "${JQ_ARGS[@]}")"  ; then
		echo "${jq_output}" > "${config_file_abs}"
		return 0
	else
		echo "Unable to update '${config_file_abs}' with new script configuration. Configuration state might be inconsistent."
		return 1
	fi
}
export __lpm_update

#
# Function: __lpm_get_state
#
# Parameters:
#
#	${1} - Absolute path to the file to update.
#
#	${2} - Script index.
#
# Returns:
#
#	0 if update was successful. Non-0 otherwise.
#
function __lpm_get_state {
	local config_file_abs
	config_file_abs="${1}"

	local script_index
	script_index="${2}"

	local state
	if ! state="$(jq "${__lpm_jq_common_params[@]}" ".scripts[${script_index}].state" "${config_file_abs}")" ; then
		echo "Unable to get script state for index ${script_index}." 1>&2
		return 1
	fi

	echo "${state}"

	return 0
}
export __lpm_get_state

