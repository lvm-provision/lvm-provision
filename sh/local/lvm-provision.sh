#!/bin/bash -x

#
# Package: lvmprovision.local
#
# Script: lvm-provision.sh
#
# Runs provision scripts and manages LVM snapshots.
#
# Returns:
#   0 - If the process succeeded.
#   1 - If there were any error.
#
# Note that this script might reboot the host. In which case the script might not return at all.
#
# Development:
# Don't forget to use "shellcheck" to validate this script!
#
# Naming conventions:
# * *_rel* prefix: Relative path.
# * *_abs* prefix: Absolute path.
#
# TODO: Create command to revert LVM snapshot.
# * Show dialog that lists all metadata and LVM snapshots numbers.
# * Ask for confirmation.
# * Revert to that snapshot and remove the rest.
# * Reboot the system.


# ======
# Header
# ======

# Ensure script robustness
# ========================
set -o errexit	# Fail immediatelly if any command fails.
set -o errtrace # If set, any trap on ERR is inherited by shell functions, command substitutions, and commands executed in a subshell environment.
set -o pipefail	# Fail immediatelly if any pipe stage fails.
set -o nounset	# Fail immediatelly if there is a derreference attempt on an unset variable. 

on_error() {
	local origin
	if [[ "${#FUNCNAME[@]}" -gt 2 ]] ; then
		origin="${FUNCNAME[1]}"
	else
		origin="${1}"
	fi

	echo "${origin}: error on line ${2}" 1>&2
	exit 1
}

trap 'on_error ${0} ${LINENO}' ERR

# Check whether this script is being run as root.
if [[ "$EUID" -ne 0 ]] ; then
	echo "This script must be run as root." 1>&2
	exit 1
fi

# ==========================
# Input parameter validation
# ==========================

function show_help {
cat << EOF 1>&2

${0} --project-dir=<abs path>

---

--project-dir	Directory containing the project to execute.

EOF
}

# Assign parameters to variables.
# ===============================
#
# Thanks to
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
# For the insights on how to properly process parameters.
for i in "${@}" ; do
case "${i}" in
	--project-dir=*)
	project_dir="${i#*=}"
	shift
	;;
	*)
	# Unknown option
	show_help
	exit 1
	;;
esac
done

# Validate reference to this script and load static configuration.
# ================================================================
if ! this_abs="$(realpath "${0}")" ; then echo "Unable to determine absolute path of '${0}'" 1>&2 ; exit 1 ; fi
if ! this_dir_abs="$(dirname "${this_abs}")" ; then echo "Unable to determine absolute dir of '${this_abs}'" 1>&2 ; exit 1 ; fi

# shellcheck source=lvm-provision/sh/static_config.sh
source "${this_dir_abs}/../static_config.sh"

# Load additional sources
# =======================

# shellcheck source=lvm-provision/sh/local/provision-metadata.sh
source "${local_lib_script_local_provisionmetadata_abs}"

# shellcheck source=lvm-provision/sh/utils.sh
source "${local_lib_script_utils_abs}"

# Make sure all required programs are available
# =============================================
required_programs=("${AWK}" "${JO}" "${JQ}" "${SHA}")
if ! check_required_programs "${required_programs[@]}" ; then echo "One or more required programs is missing. Aborting." 1>&2 ; exit 1 ; fi

# Validate "project_dir".
# =======================
if [[ "${project_dir}x" = "x" ]] ; then
	echo "'--project-dir' option is missing."
	show_help
	exit 1
fi

if [[ ! -d "${project_dir}" ]] ; then
	echo "'${project_dir}' does not exist or is not a directory." 1>&2
	exit 1
fi

if ! project_dir_abs="$(realpath "${project_dir}")" ; then echo "Unable to determine absolute path of '${project_dir}'" 1>&2 ; exit 1 ; fi

# ================================
# Project configuration validation
# ================================

# Load project config
# ===================
if ! project_config_abs="$(realpath "${project_dir_abs}/${project_config_rel}")" ; then echo "Unable to determine absolute path of '${project_dir_abs}/${project_config_rel}'" 1>&2 ; exit 1 ; fi

if [[ ! -f "${project_config_abs}" ]] ; then
	echo "'${project_config_abs}' does not exist or is not a file." 1>&2
	exit 1
fi

# Actually load the project configuration.
# shellcheck source=lvm-tester/config.sh
source "${project_config_abs}"

# =================
# Start the process
# =================

# Initialize the library.
if ! lvm_provision_metadata_config="$(lvm_provision_metadata_init "${local_lvm_provision_metadata_dir_abs}" "${project_id}")" ; then echo "Unable to initialize the LVM-provision library." ; exit 1 ; fi

# Process provision scripts.
index=0
for script_abs in "${project_dir_abs}"/"${project_script_dir_rel}"/* ; do
	lvm_provision_metadata_config="$(lvm_provision_metadata_run "${index}" "${script_abs}" "" "${lvm_provision_metadata_config}")"

	ret="${?}"

	case "${ret}" in
		1)
		;;
		2)
		;;
		-1)
		echo "'lvm_provision_metadata_matches' failed. Aborting." 1>&2
		exit 1
		;;
		*)
		# Unexpected return value
		echo "Unexpected 'lvm_provision_metadata_matches' return value '${ret}'. Aborting." 1>&2
		exit 1
		;;
	esac

	index=$((index + 1))
done

# TODO:
# * For each script (#):
# ** Calculate hash.
# ** Compare hash with hash stored in metadata for "#".
# ** lvm_provision_metadata_matches(#, hash, script path)
# *** Stores metadata if it it doesn't exist.
# ** If it matches
# *** Check whether its snapshot exists.
# **** lvm_provision_metadata_ensure_snapshot(#)
# ***** If not, create it, and update metadata (storing snapshot name).
# *** jump to next "for" iteration ("continue").
# ** Remove all LVM snapshots from "#" (included) onwards.
# *** lvm_provision_metadata_remove_from(#)
# **** Also remove metadata.
# ** Run the script.
# ** Create snapshot.
# *** lvm_provision_metadata_create_snapshot(#)
# **** Store snapshot name.

# Everything OK, for now...
exit 0

