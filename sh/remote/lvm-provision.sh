#!/bin/bash

#
# Package: lvmprovision.remote
#
# Script: lvm-provision.sh
#
# Orchestrates the LVM provision process from a remote host.
#
# Development:
# Don't forget to use "shellcheck" to validate this script!
#
# Naming conventions:
# * *_rel* prefix: Relative path.
# * *_abs* prefix: Absolute path.

# Ensure script robustness
# ========================
set -o errexit	# Fail immediatelly if any command fails.
set -o pipefail	# Fail immediatelly if any pipe stage fails.
set -o nounset	# Fail immediatelly if there is a derreference attempt on an unset variable. 

# ==========================
# Input parameter validation
# ==========================

function show_help {
cat << EOF 1>&2

${0} --project-dir=<abs path>

---

--project-dir	Directory containing the project to execute.

EOF
}

# Assign parameters to variables.
# ===============================
#
# Thanks to
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
# For the insights on how to properly process parameters.
for i in "${@}" ; do
case "${i}" in
	--project-dir=*)
	project_dir="${i#*=}"
	shift
	;;
	*)
	# Unknown option
	show_help
	exit 1
	;;
esac
done

# Validate reference to this script and load static configuration.
# ================================================================
if ! this_abs="$(realpath "${0}")" ; then echo "Unable to determine absolute path of '${0}'" 1>&2 ; exit 1 ; fi
if ! this_dir_abs="$(dirname "${this_abs}")" ; then echo "Unable to determine absolute dir of '${this_abs}'" 1>&2 ; exit 1 ; fi

# shellcheck source=lvm-provision/sh/static_config.sh
source "${this_dir_abs}/../static_config.sh"

# Load additional sources
# =======================

# shellcheck source=lvm-provision/sh/utils.sh
source "${remote_lib_script_utils_abs}"

# Make sure all required programs are available
# =============================================
required_programs=("${DIALOG}" "${SSH}" "${RSYNC}")
if ! check_required_programs "${required_programs[@]}" ; then echo "One or more required programs is missing. Aborting." 1>&2 ; exit 1 ; fi

# Validate "project_dir".
# =======================
if [[ "${project_dir}x" = "x" ]] ; then
	echo "'--project-dir' option is missing."
	show_help
	exit 1
fi

if [[ ! -d "${project_dir}" ]] ; then
	echo "'${project_dir}' does not exist or is not a directory." 1>&2
	exit 1
fi

if ! project_dir_abs="$(realpath "${project_dir}")" ; then echo "Unable to determine absolute path of '${project_dir}'" 1>&2 ; exit 1 ; fi

# ================================
# Project configuration validation
# ================================

# Load project config
# ===================
if ! project_config_abs="$(realpath "${project_dir_abs}/${project_config_rel}")" ; then echo "Unable to determine absolute path of '${project_dir_abs}/${project_config_rel}'" 1>&2 ; exit 1 ; fi

if [[ ! -f "${project_config_abs}" ]] ; then
	echo "'${project_config_abs}' does not exist or is not a file." 1>&2
	exit 1
fi

# Actually load the project configuration.
# shellcheck source=lvm-tester/config.sh
source "${project_config_abs}"

# Validate 'target_host'
# ======================

if [[ "${target_host}x" = "x" ]] ; then
	echo "'target_host' variable not defined." 1>&2
	exit 1
fi

# Validate 'local_username'
# ======================

if [[ "${local_username}x" = "x" ]] ; then
	echo "'local_username' variable not defined." 1>&2
	exit 1
fi

# ========
# Start up
# ========

# Load SSH key if not already loaded
# ==================================
# https://unix.stackexchange.com/questions/132791/have-ssh-add-be-quiet-if-key-already-there
if ! project_ssh_private_key_abs="$(realpath "${project_dir_abs}/${project_ssh_private_key_rel}")" ; then echo "Unable to determine absolute path of '${project_ssh_private_key_rel}'" 1>&2 ; exit 1 ; fi
ssh-add -l | grep -q "$(ssh-keygen -lf "${project_ssh_private_key_abs}" | awk '{print $2}')" || ssh-add "${project_ssh_private_key_abs}"

# Send the scripts to the remote host
# ===================================

# First make sure that the target directory exists.
SSH_ARGS=()

# Target host
SSH_ARGS=("${local_username}@${target_host}")

# Command
# The "rm" makes sure that the target name is not a file, by mistake.
SSH_ARGS+=("[[" "-d" "${local_script_dir_abs}" "]]" "||" "(" "rm" "-f" "${local_script_dir_abs}" "&&" "mkdir" "-p" "${local_script_dir_abs}" ")")

if ! "${SSH}" "${SSH_ARGS[@]}" ; then echo "Unable to create directory '${local_script_dir_abs}' on remote host." 1>&2 ; exit 1 ; fi

# Actually run rsync
RSYNC_ARGS=()

# Options
RSYNC_ARGS+=(--archive)
RSYNC_ARGS+=(--delete)

# Sources
RSYNC_ARGS+=("${remote_lib_dir_abs}")
RSYNC_ARGS+=("${project_dir_abs}")

# Destination
RSYNC_ARGS+=("${local_username}@${target_host}:${local_script_dir_abs}")


# Function: pause
#
# Pauses script execution for an ammount of seconds specified in configuration by the 'pause_seconds' variable and shows a dialog.
#
# Parameters:
#
#   ${1} - Text to display.
#
# Returns:
#
# Nothing.
#
function pause {
	# TODO: Remove cancel button.
	# TODO: Clear screen after dialog is completed.
	# TODO: Change colors if there are errors.
	DIALOG_ARGS=()
	DIALOG_ARGS+=("--pause" "${1}\\nPausing for ${pause_seconds}" 20 50 "${pause_seconds}")
	"${DIALOG}" "${DIALOG_ARGS[@]}"
}

# Determine the absolute project dir on the target host.
local_project_dir_abs="${local_script_dir_abs}/$(basename "${project_dir_abs}")"

SSH_ARGS=()
# Target host
SSH_ARGS=("${local_username}@${target_host}")

# Command
# Run the script on the target host.
SSH_ARGS+=("${local_lvm_provision_local_abs}" "--project-dir=${local_project_dir_abs}")

# * Somehow check that all programs are correctly installed in the target host (particularly rsync)

while true ; do
	if ! "${RSYNC}" "${RSYNC_ARGS[@]}" ; then
		pause "Unable to rsync scripts to remote host."
		continue
	fi
	"${SSH}" "${SSH_ARGS[@]}" 
	ssh_exit_status="${?}"
	case "${ssh_exit_status}" in
		0) 
			# The program was successful.
			exit 0
		;;
		1) 
			# There was an error in the script. Abort.
			exit 1
		;;
		255) 
			# There was an SSH error. Keep trying.
			pause "There was an SSH error."
			continue
		;;
		*)
			echo "The command executed in the remote host returned unexpected value '${ssh_exit_status}'. Aborting."
			exit 1
		;;
	esac
done

